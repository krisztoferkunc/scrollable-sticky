import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
} from '@angular/core';
import {
  animationFrameScheduler,
  combineLatest,
  fromEvent,
  Subject,
} from 'rxjs';
import {
  auditTime,
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  share,
  startWith,
  takeUntil,
} from 'rxjs/operators';

export enum EDirection {
  Up,
  Down,
  Left,
  Right,
}

//  -------------------------  NOTE  ----------------------------------------
//  -------------------------------------------------------------------------
//  In order to work this properly, you have to set the host element position to sticky
//  and fulfill everything, that sticky property needs (no overflow property on parents,
//  align-self: flex-start (or smth) (if parent is flex), etc.)
//  full list: https://www.designcise.com/web/tutorial/how-to-fix-issues-with-css-position-sticky-not-working
//  -------------------------------------------------------------------------
//  -------------------------------------------------------------------------

@Directive({
  selector: '[appScrollableSticky]',
})
export class ScrollableStickyDirective
  implements OnInit, AfterViewInit, OnDestroy
{
  constructor(
    private renderer: Renderer2,
    private element: ElementRef,
    private ngZone: NgZone
  ) {}

  @Input()
  public containerStickyTopGap = 60;
  @Input()
  public containerStickyBottomGap = 60;
  @Input()
  public pageOffsetTop = 0;

  private nativeElement!: HTMLElement;

  private shouldScroll = false;

  public containerHeightDiff = 0;
  public siblingId = '';
  public siblingDiv!: HTMLElement;

  private readonly onDestroy$ = new Subject<void>();

  private readonly initElements$ = new Subject<void>();

  ngOnInit(): void {
    this.nativeElement = this.element.nativeElement;
    this.initSiblingDiv();
  }

  ngAfterViewInit() {
    this.initElements();
    this.scrollHandler();
    this.resizeHandler();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
    this.initElements$.complete();
  }

  private initSiblingDiv(): void {
    this.siblingDiv = this.renderer.createElement('div');
    this.siblingId = `stickyId`;
    this.renderer.setAttribute(this.siblingDiv, 'id', this.siblingId);
    this.renderer.insertBefore(
      this.nativeElement.parentNode,
      this.siblingDiv,
      this.nativeElement
    );
  }

  /**
   * Resets the scrollable sticky element
   */
  public initElements(): void {
    // px difference between the height of the element and height of the window
    this.containerHeightDiff = Math.max(
      0,
      this.nativeElement.offsetHeight - window.innerHeight
    );

    if (this.nativeElement.offsetHeight > window.innerHeight) {
      this.shouldScroll = true;
      this.nativeElement.style.top = '';
    } else {
      this.shouldScroll = false;
      this.siblingDiv.style.marginTop = '0';
      this.nativeElement.style.top = `${this.containerStickyTopGap}px`;
      this.nativeElement.style.bottom = '';
    }
    this.initElements$.next();
  }

  // Recalculate the placeholder div's top margin (somehow)
  private recalculateUp(): void {
    this.siblingDiv.style.marginTop = `${Math.max(
      0,
      window.scrollY + this.containerStickyTopGap - this.pageOffsetTop
    )}px`;
  }

  // Recalculate the placeholder div's top margin (somehow)
  private recalculateDown(): void {
    this.siblingDiv.style.marginTop = `${Math.max(
      0,
      window.scrollY -
        this.containerHeightDiff -
        this.pageOffsetTop -
        this.containerStickyBottomGap
    )}px`;
  }

  private scrollUpCalc(): void {
    if (
      Math.abs(this.nativeElement.getBoundingClientRect().top) >=
      this.containerHeightDiff + this.containerStickyBottomGap - 1
    ) {
      // if the sticky element was docked to the bottom
      this.recalculateDown();
    }

    this.nativeElement.style.top = '';
    this.nativeElement.style.bottom = `-${
      this.containerHeightDiff + this.containerStickyTopGap
    }px`;
  }

  private scrollDownCalc(): void {
    if (
      Math.abs(this.nativeElement.getBoundingClientRect().bottom) >
      this.nativeElement.offsetHeight + this.containerStickyTopGap - 12
    ) {
      // if the sticky element was docked to the top
      this.recalculateUp();
    }

    this.nativeElement.style.bottom = '';
    this.nativeElement.style.top = `-${
      this.containerHeightDiff + this.containerStickyBottomGap
    }px`;
  }

  private scrollHandler(): void {
    this.ngZone.runOutsideAngular(() => {
      const scroll$ = fromEvent(window, 'scroll').pipe(
        auditTime(0, animationFrameScheduler),
        filter(() => this.shouldScroll === true),
        map(() => window.pageYOffset),
        pairwise(),
        map(
          ([y1, y2]): EDirection => (y2 < y1 ? EDirection.Up : EDirection.Down)
        ),
        distinctUntilChanged(),
        share(),
        takeUntil(this.onDestroy$)
      );

      const scrollWithInit$ = combineLatest([
        scroll$,
        this.initElements$.pipe(startWith(null)),
      ]).pipe(map(([direction]) => direction));

      const scrollUpWithInit$ = scrollWithInit$.pipe(
        filter((direction) => direction === EDirection.Up)
      );

      const scrollDownWithInit$ = scrollWithInit$.pipe(
        filter((direction) => direction === EDirection.Down)
      );

      scrollUpWithInit$.subscribe(() => {
        this.ngZone.run(() => this.scrollUpCalc());
      });

      scrollDownWithInit$.subscribe(() => {
        this.ngZone.run(() => this.scrollDownCalc());
      });
    });
  }

  private resizeHandler(): void {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(window, 'resize')
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(() => {
          this.ngZone.run(() => {
            this.initElements();
          });
        });
    });
  }
}
