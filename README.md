## Scrollable sticky

In this repository, you will find a directive that encapsulates all the necessary logic to create a scrollable, sticky sidebar. Additionally, there is a demo project included for you to test and experience the results for yourself.

Here's a quick preview of what you can expect:

<img src="/demo.gif?raw=true" alt="demo gif" width="900px">

As you can see, the core of this solution is that the sidebar "sticks" to the top when you scroll down and "sticks" to the bottom when you scroll up.
